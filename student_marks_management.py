import mysql.connector as sql
import sys
import os

# Disable output buffering
os.environ['PYTHONUNBUFFERED'] = '1'


# Connect to MySQL database
db = sql.connect(host="localhost", user="Khushi", passwd="Root", database="student_marks_db")
cursor = db.cursor()

# Check if the database is connected
if db.is_connected():
    print("Database connected")

def create_table():
    try:
        print("Creating STUDENT table")
        sql = '''
            CREATE TABLE IF NOT EXISTS student (
                ROLL INT(4) PRIMARY KEY,
                name VARCHAR(50) NOT NULL,
                class CHAR(3) NOT NULL,
                sec CHAR(1),
                mark1 INT(4),
                mark2 INT(4),
                mark3 INT(4),
                mark4 INT(4),
                mark5 INT(4),
                total INT(4),
                per FLOAT(4)
            )
        '''
        cursor.execute(sql)
        db.commit()
        print("Table created successfully")
    except Exception as e:
        print("Error:", e)

def display_tables():
    try:
        cursor.execute("SHOW TABLES")
        tables = cursor.fetchall()
        for table in tables:
            print(table[0])
    except Exception as e:
        print("Error:", e)

def display_fields(table_name):
    try:
        cursor.execute(f"DESC {table_name}")
        fields = cursor.fetchall()
        for field in fields:
            print(field[0])
    except Exception as e:
        print("Error:", e)

def display_all_data():
    try:
        cursor.execute("SELECT * FROM student")
        data = cursor.fetchall()
        for row in data:
            print(*row)
    except Exception as e:
        print("Error:", e)

def add_student():
    try:
        r = int(input("Enter student roll number: "))
        name = input("Enter student name: ")
        c = input("Enter class of student: ")
        s = input("Enter section of student: ")
        marks = [int(input(f"Enter marks in SUBJECT{i + 1}: ")) for i in range(5)]
        
        t = sum(marks)
        per = t / 5
        
        query = "INSERT INTO student (ROLL, name, class, sec, mark1, mark2, mark3, mark4, mark5, total, per) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        values = (r, name, c, s, *marks, t, per)
        
        cursor.execute(query, values)
        db.commit()
        print("Student record saved in table")
    except Exception as e:
        print("Error:", e)

def search_student():
    print("1: TO SEARCH BY STUDENT ROLL NUMBER")
    print("2: TO SEARCH BY STUDENT NAME")
    c = int(input("Enter your choice: "))
    
    if c == 1:
        try:
            roll = int(input("Enter student roll number to search: "))
            query = "SELECT * FROM student WHERE ROLL = %s"
            cursor.execute(query, (roll,))
            data = cursor.fetchall()
            if not data:
                print("Student not found")
            else:
                for row in data:
                    print(*row)
        except Exception as e:
            print("Error:", e)
    elif c == 2:
        try:
            name = input("Enter student name to search: ")
            query = "SELECT * FROM student WHERE name = %s"
            cursor.execute(query, (name,))
            data = cursor.fetchall()
            if not data:
                print("Student not found")
            else:
                for row in data:
                    print(*row)
        except Exception as e:
            print("Error:", e)
    else:
        print("Invalid choice")

def update_marks():
    try:
        roll = int(input("Enter roll number of student whose marks to be updated: "))
        query = "SELECT * FROM student WHERE ROLL = %s"
        cursor.execute(query, (roll,))
        data = cursor.fetchall()
        
        if not data:
            print("Student not found")
        else:
            new_marks = [int(input(f"Enter updated marks in SUBJECT{i + 1}: ")) for i in range(5)]
            t = sum(new_marks)
            per = t / 5
            
            query = "UPDATE student SET mark1 = %s, mark2 = %s, mark3 = %s, mark4 = %s, mark5 = %s, total = %s, per = %s WHERE ROLL = %s"
            values = (*new_marks, t, per, roll)
            
            cursor.execute(query, values)
            db.commit()
            print("Student record updated")
    except Exception as e:
        print("Error:", e)

def delete_student():
    try:
        roll = int(input("Enter student roll number to delete: "))
        query = "SELECT * FROM student WHERE ROLL = %s"
        cursor.execute(query, (roll,))
        data = cursor.fetchall()
        
        if not data:
            print("Student not found in table")
        else:
            query = "DELETE FROM student WHERE ROLL = %s"
            cursor.execute(query, (roll,))
            db.commit()
            print("Student record deleted from table")
    except Exception as e:
        print("Error:", e)

# MENU FOR STUDENT MARKS MANAGEMENT SYSTEM SOFTWARE
print("*************************************************")
print("WELCOME TO MY PROJECT STUDENT MARKS MANAGEMENT SYSTEM")
print("*************************************************")

while True:
    print()
    print("1: TO CREATE TABLE FOR THE FIRST TIME")
    print("2: TO DISPLAY TABLES OF DATABASE")
    print("3: TO SHOW FIELDS OF TABLE")
    print("4: TO DISPLAY ALL DATA")
    print("5: TO ADD NEW STUDENT")
    print("6: TO SEARCH A STUDENT RECORD")
    print("7: TO CHANGE MARKS OF STUDENT")
    print("8: TO DELETE STUDENT")
    print("9: EXIT")

    try:
        choice = int(input("ENTER YOUR CHOICE: "))
        
        if choice == 1:
            create_table()
        elif choice == 2:
            display_tables()
        elif choice == 3:
            table_name = input("Enter table name: ")
            display_fields(table_name)
        elif choice == 4:
            display_all_data()
        elif choice == 5:
            add_student()
        elif choice == 6:
            search_student()
        elif choice == 7:
            update_marks()
        elif choice == 8:
            delete_student()
        elif choice == 9:
            db.close()
            sys.exit()
        else:
            print("Invalid choice")
    except ValueError:
        print("Invalid input. Please enter a number.")
    except KeyboardInterrupt:
        db.close()
        sys.exit()

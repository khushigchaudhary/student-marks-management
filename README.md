##Title of the Project:
Student Marks Management System

##About the project
This project is designed to add a new student, update marks or details of existing student, delete details or to delete the whole table; it is also capable of searching a student’s details.

##Reason for choosing the project
This project involves connecting to and interacting with a MySQL database. This offers an opportunity to learn about database design, CRUD (Create, Read, Update, Delete) operations, and data storage.
It also involves solving a specific problem—managing and organizing student marks. This encourages critical thinking and problem-solving skills as you design and implement solutions to manage student records effectively.

##Requirements
This project is made by SQL DBMS and Python connectivity using “mysql.connector” module. The module “sys” is used to provide access to some variables used or maintained by the interpreter and to functions that interact strongly with interpreter.
Python 3.6.x or higher version - To run the program 
MySQL- To run the database of the project
MySQL Connector- For making connection between Python and MySQL.


